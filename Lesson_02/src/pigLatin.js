/*
Свиняча латина - це спосіб зміни англійських слів. Правила такі:

- Якщо слово починається на приголосну, візьміть першу приголосну або збіг приголосних, перемістіть її в кінець слова та додайте до неї "ay".

- Якщо слово починається з голосної, просто додайте "way" у кінці.

Перекладіть наданий рядок на свинячу латину. Гарантовано, що вхідними рядками є англійські слова з великої літери.
*/

function translatePigLatin(str) {

    let prefix =[], suffix = [];

    const vowel = ["a", "e", "i", "o", "u", 'y']

    if (vowel.indexOf(str[0]) != -1) {
        return str+'way'
    } else {
        for (let i = 0 ; i < str.length ; i++) {
                if (vowel.indexOf(str[i]) == -1) {
                    prefix.push(str[i])
                } else {
                    suffix = str.slice(i)
                    str = []
                }
            }

      return suffix+prefix.join('')+'ay';
    }
}

console.log(translatePigLatin("consonant"));