/*
Суті завдання не зрозумів:
Fill in the object constructor with the following methods below:
*/

var Person = function(firstAndLast) {
  // Only change code below this line
  // Complete the method below and implement the others similarly
  this.getFullName = function() {
    return firstAndLast;
  };

  this.getFirstName = () => {
    return firstAndLast.split(' ')[0]
       };

  this.getLastName = () => {
    return firstAndLast.split(' ')[1]
       };

  this.setFirstName = (first) => {
    tmp = firstAndLast.split(' ')
    tmp[0] = first.split(' ')[0]
    return tmp.join(' ')
       };

  this.setLastName = (last) => {
    tmp = firstAndLast.split(' ')
    tmp[1] = last.split(' ')[1]
    return tmp.join(' ')
       };

  this.setFullName = (firstAndLast) => {
    this.firstAndLast = firstAndLast
    return this.firstAndLast
       };

};

var bob = new Person('Bob Ross');
console.log('First Name: ' + bob.getFirstName());
console.log('Last Name: ' + bob.getLastName());
console.log('Full Name: ' + bob.getFullName());

console.log('New First Name: ' + bob.setFirstName('John Doe'));
console.log('New Last Name: ' + bob.setLastName('John Doe'));
console.log('New Full Name: ' + bob.setFullName('John Doe'));
