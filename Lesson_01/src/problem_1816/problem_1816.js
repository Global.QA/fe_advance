/**
 * @param {string} s
 * @param {number} k
 * @return {string}
 */

var truncateSentence = function(s, k) {
return s.split(" ", k).join(" ")
};


console.log(truncateSentence("Do you really want to hurt me", 4))